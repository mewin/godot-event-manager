tool
extends EditorPlugin

class_name GEMEditorPlugin

const __INSTANCE_META_KEY = "__gem_instance__"

enum ComponentType {
	TRIGGER,
	CONDITION,
	ACTION
}

var __event_manager : Control
var __event_inspector_plugin : EditorInspectorPlugin = load("res://addons/de.mewin.godot-event-manager/scripts/plugins/event_inspector_plugin.gd").new()
var __trigger_types := {}
var __condition_types := {}
var __action_types := {}

#############
# overrides #
#############
func _enter_tree():
	__setup_editor()
	
	get_tree().set_meta(__INSTANCE_META_KEY, self)
	add_inspector_plugin(__event_inspector_plugin)
	add_trigger_type(preload("res://addons/de.mewin.godot-event-manager/scripts/types/triggers/signal_trigger.gd"))
	add_action_type(preload("res://addons/de.mewin.godot-event-manager/scripts/types/actions/call_method_action.gd"))
	add_action_type(preload("res://addons/de.mewin.godot-event-manager/scripts/types/actions/set_property_action.gd"))
	add_condition_type(preload("res://addons/de.mewin.godot-event-manager/scripts/types/conditions/expression_condition.gd"))

func _exit_tree():
	get_tree().remove_meta(__INSTANCE_META_KEY)
	remove_inspector_plugin(__event_inspector_plugin)

func get_plugin_name():
	return tr("Event Manager")

func get_plugin_icon():
	return preload("res://addons/de.mewin.godot-event-manager/images/gem.svg")

func has_main_screen():
	return true

func make_visible(visible : bool):
	if visible:
		__remove_editor() # TODO: only for development
		__setup_editor()
	__event_manager.visible = visible

func get_state():
	return {
		"event": __event_manager.event
	}

func set_state(state : Dictionary):
	if state.has("event"):
		__event_manager.event = state["event"]

#################
# private stuff #
#################
func __setup_editor():
	var editor_interface := get_editor_interface()
	__event_manager = load("res://addons/de.mewin.godot-event-manager/scenes/event_manager.tscn").instance()
	__event_manager.visible = false
	editor_interface.get_editor_viewport().add_child(__event_manager)

func __remove_editor():
	__event_manager.queue_free()
	__event_manager = null

func __filter_event_controller(node : Node):
	return node is GEMEventController && node.owner == get_editor_interface().get_edited_scene_root()

################
# public stuff #
################
func add_trigger_type(trigger_type):
	var type_name : String = trigger_type._get_type_name()
	if !__trigger_types.has(type_name):
		__trigger_types[type_name] = trigger_type

func remove_trigger_type(trigger_type):
	var type_name : String = trigger_type._get_type_name()
	__trigger_types.erase(type_name)

func get_trigger_types() -> Dictionary:
	return __trigger_types.duplicate()

func get_trigger_type(trigger):
	return __trigger_types[trigger._get_type_name()]

func add_condition_type(condition_type):
	var type_name : String = condition_type._get_type_name()
	if !__condition_types.has(type_name):
		__condition_types[type_name] = condition_type

func remove_condition_type(condition_type):
	var type_name : String = condition_type._get_type_name()
	__condition_types.erase(type_name)

func get_condition_types() -> Dictionary:
	return __condition_types.duplicate()

func get_condition_type(condition : GEMCondition):
	return __condition_types[condition._get_type_name()]
func add_action_type(action_type):
	var type_name : String = action_type._get_type_name()
	if !__action_types.has(type_name):
		__action_types[type_name] = action_type

func remove_action_type(action_type):
	var type_name : String = action_type._get_type_name()
	__action_types.erase(type_name)

func get_action_types() -> Dictionary:
	return __action_types.duplicate()

func get_action_type(action):
	return __action_types[action._get_type_name()]

func get_component_types(type : int) -> Dictionary:
	match type:
		ComponentType.TRIGGER:
			return get_trigger_types()
		ComponentType.CONDITION:
			return get_condition_types()
		ComponentType.ACTION:
			return get_action_types()
	assert(false)
	return {}

func get_comp_type(comp):
	if comp is GEMAction:
		return get_action_type(comp)
	elif comp is GEMCondition:
		return get_condition_type(comp)
	elif comp is GEMTrigger:
		return get_trigger_type(comp)
	assert(false)
	return null

func find_event_controller(create := false) -> GEMEventController:
	var scene_root := get_editor_interface().get_edited_scene_root()
	if !scene_root:
		return null
	var event_controller : GEMEventController = GDBUtility.find_node_filter(scene_root, self, "__filter_event_controller")
	if create && !event_controller:
		event_controller = GEMEventController.new()
		event_controller.name = tr("EventController")
		scene_root.add_child(event_controller)
		event_controller.owner = scene_root
		print(tr("Automatically created event controller node."))
	return event_controller

static func instance():
	var tree := Engine.get_main_loop() as SceneTree
	assert(tree && tree.has_meta(__INSTANCE_META_KEY))
	return tree.get_meta(__INSTANCE_META_KEY)
