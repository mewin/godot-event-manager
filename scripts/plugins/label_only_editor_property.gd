extends EditorProperty

class_name GEMLabelOnlyEditorProperty

func _init(label_ : String):
	label = label_
	read_only = true
