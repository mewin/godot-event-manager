tool
extends Button

func _ready():
	text = tr("Show Event Manager")
	icon = preload("res://addons/de.mewin.godot-event-manager/images/gem.svg")

func _pressed():
	var plugin : GEMEditorPlugin = GEMEditorPlugin.instance()
	plugin.get_editor_interface().set_main_screen_editor(plugin.get_plugin_name())
