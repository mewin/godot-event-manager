tool
extends EditorInspectorPlugin

class_name GEMEventInspectorPlugin

func can_handle(object):
	return true

func parse_begin(object):
	if object is GEMEventController:
		add_custom_control(load("res://addons/de.mewin.godot-event-manager/scripts/plugins/event_manager_button.gd").new())
	else:
		__parse_generic_object(object)

func parse_property(object, type : int, path : String, hint : int, hint_text : String, usage : int):
	if object is GEMEventController:
		if path == "events":
			return true
		return false
	return false

func parse_end():
	pass

func __parse_generic_object(object):
	var event_controller : GEMEventController = GEMEditorPlugin.instance().find_event_controller()
	if !event_controller:
		return
