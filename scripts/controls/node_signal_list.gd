tool
extends Tree

var signals := [] setget _set_signals
var selected_signal := "" setget _set_selected_signal

func _ready():
	columns = 1
	select_mode = SELECT_ROW
	allow_reselect = true
	__fill()
	connect("item_selected", self, "_on_item_selected")

func __fill():
	clear()
	
	create_item() # root
	
	var selected_found := false
	for sig in signals:
		if !sig is Dictionary || !sig.has("name"):
			printerr("NodeSignalList: invalid element in signal list")
			continue
		
		var itm := create_item()
		itm.set_text(0, GDBUtility.format_signal_signature(sig))
#		itm.set_text(1, GDBUtility.type_name(property["type"]))
		itm.set_metadata(0, sig)
		if selected_signal && sig["name"] == selected_signal:
			itm.select(0)
			selected_found = true
	if !selected_found:
		selected_signal = ""
		emit_signal("selected_signal_changed")

func __select_signal(root : TreeItem, sig : String) -> bool:
	if root == null:
		return false
	var meta = root.get_metadata(0)
	if meta is Dictionary && meta["name"] == sig:
		root.select(0)
		return true
	if __select_signal(root.get_children(), sig):
		return true
	else:
		return __select_signal(root.get_next(), sig)

func _set_signals(value : Array):
	if value != signals:
		signals = value
		__fill()

func _set_selected_signal(value : String):
	if value == selected_signal:
		return
	if __select_signal(get_root(), value):
		selected_signal = value
	elif selected_signal:
		var item := get_selected()
		if item:
			item.deselect(0)
		selected_signal = ""

func _on_item_selected():
	var item := get_selected()
	if !item:
		return
	var meta = item.get_metadata(0)
	if meta is Dictionary:
		selected_signal = meta["name"]
		emit_signal("selected_signal_changed")

signal selected_signal_changed()
