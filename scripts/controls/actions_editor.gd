tool
extends "res://addons/de.mewin.godot-event-manager/scripts/controls/event_comp_editor.gd"

func _get_tree() -> Tree:
	return $tree_actions as Tree

func _get_edit_dialog() -> WindowDialog:
	return $edit_comp_dialog as WindowDialog

func _get_comps():
	return event.actions

func _append_comp(comp):
	event.append_action(comp)
