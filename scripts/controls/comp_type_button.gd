tool
extends OptionButton

export(GEMEditorPlugin.ComponentType) var component_type = GEMEditorPlugin.ComponentType.TRIGGER

var comp_type setget _set_comp_type

#############
# overrides #
#############
func _ready():
	var popup := get_popup()
	popup.connect("about_to_show", self, "_on_about_to_show")
	popup.connect("index_pressed", self, "_on_index_pressed")
	
	if comp_type:
		text = comp_type._get_display_name()
	else:
		text = tr("<select type>")

###########
# setters #
###########
func _set_comp_type(value):
	if value != comp_type:
		comp_type = value
		if comp_type:
			text = comp_type._get_display_name()
		else:
			text = tr("<select type>")
		emit_signal("comp_type_changed")

############
# handlers #
############
func _on_about_to_show():
	var popup := get_popup()
	
	popup.clear()
	var comp_types : Dictionary = GEMEditorPlugin.instance().get_component_types(component_type)
	for type_name in comp_types:
		var comp_type_ = comp_types[type_name]
		var idx := popup.get_item_count()
		popup.add_item(comp_type_._get_display_name())
		popup.set_item_metadata(idx, comp_type_)

func _on_index_pressed(idx : int):
	_set_comp_type(get_popup().get_item_metadata(idx))

###########
# setters #
###########
signal comp_type_changed()
