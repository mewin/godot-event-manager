tool
extends GEMComponentEditor

onready var tree_node := $grid_container/tree_node
onready var tree_signal := $grid_container/tree_signal
onready var edt_node := $grid_container/edt_node
onready var edt_signal := $grid_container/edt_signal
onready var common_trigger_editor := $common_trigger_editor

var trigger : GEMSignalTrigger

func _ready():
	if trigger:
		tree_node.selected_node = trigger.node
		edt_node.text = str(trigger.node)
		__update_signal_list()
		
		edt_signal.text = trigger.signal_name
		tree_signal.selected_signal = trigger.signal_name
		common_trigger_editor.from_trigger(trigger)

func _is_valid() -> bool:
	return true

func _apply(undo_redo : UndoRedo) -> bool:
	assert(trigger)
	undo_redo.create_action(tr("Signal trigger changed"))
	undo_redo.add_do_property(trigger, "node", NodePath(edt_node.text))
	undo_redo.add_do_property(trigger, "signal_name", edt_signal.text)
	undo_redo.add_undo_property(trigger, "node", trigger.node)
	undo_redo.add_undo_property(trigger, "signal_name", trigger.signal_name)
	common_trigger_editor.to_trigger(undo_redo, trigger)
	undo_redo.commit_action()
	return true

func __compare_signals(signal0 : Dictionary, signal1 : Dictionary):
	return signal0["name"] < signal1["name"]

func __update_signal_list():
	var node := get_tree().edited_scene_root.get_node_or_null(tree_node.selected_node)
	if node:
		var signals := node.get_signal_list()
		signals.sort_custom(self, "__compare_signals")
		tree_signal.signals = signals
	else:
		tree_signal.signals = []

func _on_tree_node_selected_node_changed():
	if !edt_node:
		return
	__update_signal_list()
	edt_node.text = str(tree_node.selected_node)
	emit_signal("changed")

func _on_edt_node_text_changed(new_text : String):
	if !tree_node:
		return
	var root := get_tree().edited_scene_root
	var node := root.get_node_or_null(new_text)
	if node:
		tree_node.set_block_signals(true)
		tree_node.selected_node = root.get_path_to(node)
		tree_node.set_block_signals(false)
		__update_signal_list()
	emit_signal("changed")

func _on_edt_signal_text_changed(new_text : String):
	if !tree_signal:
		return
	tree_signal.set_block_signals(true)
	tree_signal.selected_signal = new_text
	tree_signal.set_block_signals(false)
	emit_signal("changed")

func _on_tree_signal_selected_signal_changed():
	if !edt_signal:
		return
	edt_signal.text = tree_signal.selected_signal
	emit_signal("changed")

func _on_common_trigger_editor_changed():
	emit_signal("changed")
