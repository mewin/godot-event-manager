tool
extends GEMComponentEditor

var condition : GEMExpressionCondition

onready var opt_operation := $h_box_container/opt_operation
onready var edt_expression0 := $h_box_container/v_box_container/edt_expression0
onready var edt_expression1 := $h_box_container/v_box_container2/edt_expression1
onready var tree_expression0 := $h_box_container/v_box_container/tree_expression0
onready var tree_expression1 := $h_box_container/v_box_container2/tree_expression1
onready var common_condition_editor := $common_condition_editor

func _ready():
	if condition:
		opt_operation.operation_type = condition.operation
		edt_expression0.text = condition.expression0
		edt_expression1.text = condition.expression1
		common_condition_editor.from_condition(condition)
		
		__update_tree_selection(edt_expression0, tree_expression0)
		__update_tree_selection(edt_expression1, tree_expression1)

func _apply(undo_redo : UndoRedo) -> bool:
	undo_redo.create_action(tr("Expression condition changed"))
	undo_redo.add_do_property(condition, "operation", opt_operation.operation_type)
	undo_redo.add_do_property(condition, "expression0", edt_expression0.text)
	undo_redo.add_do_property(condition, "expression1", edt_expression1.text)
	undo_redo.add_undo_property(condition, "operation", condition.operation)
	undo_redo.add_undo_property(condition, "expression0", condition.expression0)
	undo_redo.add_undo_property(condition, "expression1", condition.expression1)
	common_condition_editor.to_condition(undo_redo, condition)
	undo_redo.commit_action()
	return true

func __update_tree_selection(line_edit : LineEdit, tree : EditorNodeTree):
	var txt := line_edit.text
	if txt.begins_with("$") && txt.find(" ") < 0:
		var node_path := NodePath(txt.substr(1))
		if node_path.is_absolute():
			node_path = NodePath(str(node_path).substr(1)) # remove "/"
		tree.selected_node = node_path

func _on_opt_operation_operation_type_changed():
	emit_signal("changed")

func _on_edt_expression0_text_changed(new_text):
	__update_tree_selection(edt_expression0, tree_expression0)
	emit_signal("changed")

func _on_edt_expression1_text_changed(new_text):
	__update_tree_selection(edt_expression1, tree_expression1)
	emit_signal("changed")

func _on_common_condition_editor_changed():
	emit_signal("changed")

func _on_tree_expression0_item_activated():
	edt_expression0.text = "$%s" % tree_expression0.selected_node

func _on_tree_expression1_item_activated():
	edt_expression1.text = "$%s" % tree_expression1.selected_node
