tool
extends Control

onready var chk_disabled := $chk_disabled

func from_condition(condition : GEMCondition):
	chk_disabled.pressed = condition.disabled

func to_condition(undo_redo : UndoRedo, condition : GEMCondition):
	undo_redo.add_do_property(condition, "disabled", chk_disabled.pressed)
	undo_redo.add_undo_property(condition, "disabled", condition.disabled)
	
	# apparently this isnt emitted automatically
	undo_redo.add_do_method(condition, "emit_signal", "changed")
	undo_redo.add_undo_method(condition, "emit_signal", "changed")

func _on_chk_toggled(button_pressed):
	emit_signal("changed")

signal changed()
