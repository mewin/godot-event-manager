tool
extends Tree

var properties := [] setget _set_properties
var selected_property := "" setget _set_selected_property

func _ready():
	columns = 2
	select_mode = SELECT_ROW
	allow_reselect = true
	__fill()
	connect("item_selected", self, "_on_item_selected")

func __fill():
	clear()
	
	create_item() # root
	
	var selected_found := false
	for property in properties:
		if !property is Dictionary || !property.has("name") || !property.has("type"):
			printerr("NodePropertyList: invalid element in property list")
			continue
		
		var itm := create_item()
		itm.set_text(0, property["name"])
		itm.set_text(1, GDBUtility.type_name(property["type"]))
		itm.set_metadata(0, property)
		if selected_property && property["name"] == selected_property:
			itm.select(0)
			selected_found = true
	if !selected_found:
		selected_property = ""
		emit_signal("selected_property_changed")

func __select_property(root : TreeItem, property : String) -> bool:
	if root == null:
		return false
	var meta = root.get_metadata(0)
	if meta is Dictionary && meta["name"] == property:
		root.select(0)
		return true
	if __select_property(root.get_children(), property):
		return true
	else:
		return __select_property(root.get_next(), property)

func _set_properties(value : Array):
	if value != properties:
		properties = value
		__fill()

func _set_selected_property(value : String):
	if value == selected_property:
		return
	if __select_property(get_root(), value):
		selected_property = value
	elif selected_property:
		var item := get_selected()
		if item:
			item.deselect(0)
		selected_property = ""

func _on_item_selected():
	var item := get_selected()
	if !item:
		return
	var meta = item.get_metadata(0)
	if meta is Dictionary:
		selected_property = meta["name"]
		emit_signal("selected_property_changed")

signal selected_property_changed()
