tool
extends Tree

var methods := [] setget _set_methods
var selected_method := "" setget _set_selected_method

func _ready():
	columns = 1
	select_mode = SELECT_ROW
	allow_reselect = true
	__fill()
	connect("item_selected", self, "_on_item_selected")

func __fill():
	clear()
	
	create_item() # root
	
	var selected_found := false
	for method in methods:
		if !method is Dictionary || !method.has("name"):
			printerr("NodeMethodlist: invalid element in method list")
			continue
		
		var itm := create_item()
		itm.set_text(0, GDBUtility.format_method_signature(method))
#		itm.set_text(1, GDBUtility.type_name(property["type"]))
		itm.set_metadata(0, method)
		if selected_method && method["name"] == selected_method:
			itm.select(0)
			selected_found = true
	if !selected_found:
		selected_method = ""
		emit_signal("selected_method_changed")

func __select_method(root : TreeItem, method : String) -> bool:
	if root == null:
		return false
	var meta = root.get_metadata(0)
	if meta is Dictionary && meta["name"] == method:
		root.select(0)
		return true
	if __select_method(root.get_children(), method):
		return true
	else:
		return __select_method(root.get_next(), method)

func _set_methods(value : Array):
	if value != methods:
		methods = value
		__fill()

func _set_selected_method(value : String):
	if value == selected_method:
		return
	if __select_method(get_root(), value):
		selected_method = value
	elif selected_method:
		var item := get_selected()
		if item:
			item.deselect(0)
		selected_method = ""

func _on_item_selected():
	var item := get_selected()
	if !item:
		return
	var meta = item.get_metadata(0)
	if meta is Dictionary:
		selected_method = meta["name"]
		emit_signal("selected_method_changed")

signal selected_method_changed()
