tool
extends Control

class __EventCreateAction:
	extends Object
	
	var event_controller : GEMEventController
	var event : GEMEvent
	
	func do():
		event_controller.add_event(event)
	
	func undo():
		event_controller.erase_event(event)

class __EventRemoveAction:
	extends Object
	
	var event_controller : GEMEventController
	var event : GEMEvent
	var idx : int
	
	func do():
		idx = event_controller.events.find(event)
		if idx >= 0:
			event_controller.erase_event(event)
	
	func undo():
		if idx >= 0:
			event_controller.add_event(event, idx)

onready var opt_events := $v_box_container/h_box_container/opt_events
onready var cnt_components := $v_box_container/cnt_components
onready var triggers_editor := $v_box_container/cnt_components/triggers_editor
onready var conditions_editor := $v_box_container/cnt_components/conditions_editor
onready var actions_editor := $v_box_container/cnt_components/actions_editor
onready var lbl_no_event_selected := $v_box_container/lbl_no_event_selected
onready var dialog_event_label := $dialog_event_label
onready var btn_rename := $v_box_container/h_box_container/btn_rename
onready var btn_remove := $v_box_container/h_box_container/btn_remove

var event : GEMEvent setget _set_event
var __creating_event := false

func _ready():
	var plugin : GEMEditorPlugin = GEMEditorPlugin.instance()
	plugin.connect("scene_changed", self, "_on_scene_changed")
	
	__init()

func __init():
	var event_controller : GEMEventController = GEMEditorPlugin.instance().find_event_controller()
	if !event_controller || !event_controller.events:
		_set_event(null)
	else:
		_set_event(event_controller.events[0])
		if !event_controller.is_connected("events_changed", self, "_on_events_changed"):
			event_controller.connect("events_changed", self, "_on_events_changed")
	__update()

func __update():
	opt_events.event = event
	triggers_editor.event = event
	conditions_editor.event = event
	actions_editor.event = event
	btn_rename.disabled = (event == null)
	btn_remove.disabled = (event == null)
	cnt_components.visible = (event != null)
	lbl_no_event_selected.visible = !cnt_components.visible

func _on_scene_changed(root : Node):
	__init()

func _on_btn_add_pressed():
	var event_controller : GEMEventController = GEMEditorPlugin.instance().find_event_controller(true)
	if !event_controller:
		return
	elif !event_controller.is_connected("events_changed", self, "_on_events_changed"):
		event_controller.connect("events_changed", self, "_on_events_changed")
	
	__creating_event = true
	dialog_event_label.get_line_edit().text = tr("New Event")
	dialog_event_label.rect_size = dialog_event_label.rect_min_size
	dialog_event_label.popup_centered()

func _on_btn_rename_pressed():
	if !event:
		return
	__creating_event = false
	dialog_event_label.get_line_edit().text = event.label
	dialog_event_label.rect_size = dialog_event_label.rect_min_size
	dialog_event_label.popup_centered()

func _on_btn_remove_pressed():
	if !event:
		return
	var event_controller : GEMEventController = GEMEditorPlugin.instance().find_event_controller()
	if !event_controller:
		return
	var undo_redo : UndoRedo = GEMEditorPlugin.instance().get_undo_redo()
	var remover := __EventRemoveAction.new()
	
	remover.event_controller = event_controller
	remover.event = event
	
	undo_redo.create_action(tr("Remove event"))
	undo_redo.add_do_method(remover, "do")
	undo_redo.add_undo_method(remover, "undo")
	undo_redo.add_do_reference(remover)
	undo_redo.add_undo_reference(remover)
	undo_redo.commit_action()

func _on_dialog_event_label_confirmed():
	var new_label : String = dialog_event_label.get_line_edit().text.strip_edges()
	
	if !new_label:
		printerr("invalid (empty) event label")
		return
	
	var undo_redo : UndoRedo = GEMEditorPlugin.instance().get_undo_redo()
	
	if __creating_event:
		var event_controller : GEMEventController = GEMEditorPlugin.instance().find_event_controller()
		var new_event := GEMEvent.new()
		var creater := __EventCreateAction.new()
		
		assert(event_controller)
		
		new_event.label = new_label
		creater.event_controller = event_controller
		creater.event = new_event
		
		undo_redo.create_action(tr("Create event"))
		undo_redo.add_do_method(creater, "do")
		undo_redo.add_undo_method(creater, "undo")
		undo_redo.add_do_reference(creater)
		undo_redo.add_undo_reference(creater)
		undo_redo.commit_action()
		
		_set_event(new_event)
	else:
		assert(event)
		undo_redo.create_action(tr("Rename event"))
		undo_redo.add_do_property(event, "label", new_label)
		undo_redo.add_undo_property(event, "label", event.label)
		undo_redo.commit_action()

func _on_opt_events_event_selected():
	_set_event(opt_events.event)

func _on_events_changed():
	var event_controller : GEMEventController = GEMEditorPlugin.instance().find_event_controller()
	assert(event_controller)
	
	if !event in event_controller.events:
		if event_controller.events:
			event = event_controller.events[0]
		else:
			event = null
	__update()

func _set_event(value : GEMEvent):
	if value != event:
		event = value
		__update()
