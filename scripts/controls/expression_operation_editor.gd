tool
extends GEMComponentEditor

var condition : GEMExpressionCondition

onready var opt_type0 := $opt_type0
onready var opt_type1 := $opt_type1
onready var opt_operation := $opt_operation
onready var edt_expression0 := $edt_expression0
onready var edt_expression1 := $edt_expression1

func _ready():
	if condition:
		opt_type0.expression_type = condition.expression_type0
		opt_type1.expression_type = condition.expression_type1
		opt_operation.operation_type = condition.operation
		edt_expression0.text = condition.expression0
		edt_expression1.text = condition.expression1

func _apply(undo_redo : UndoRedo) -> bool:
	assert(false) # unused?!
	condition.expression_type0 = opt_type0.expression_type
	condition.expression_type1 = opt_type1.expression_type
	condition.operation = opt_operation.operation_type
	condition.expression0 = edt_expression0.text
	condition.expression1 = edt_expression1.text
	return true

func _on_opt_type0_expression_type_changed():
	emit_signal("changed")

func _on_opt_type1_expression_type_changed():
	emit_signal("changed")

func _on_opt_operation_operation_type_changed():
	emit_signal("changed")

func _on_edt_expression0_text_changed(new_text):
	emit_signal("changed")

func _on_edt_expression1_text_changed(new_text):
	emit_signal("changed")
