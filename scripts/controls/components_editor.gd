tool
extends "res://addons/de.mewin.godot-event-manager/scripts/controls/event_comp_editor.gd"

export(GEMEditorPlugin.ComponentType) var component_type = GEMEditorPlugin.ComponentType.TRIGGER setget _set_component_type

#############
# overrides #
#############
func _ready():
	# call_deferred("__setup")
	pass

func _get_tree() -> Tree:
	return $tree as Tree

func _get_edit_dialog() -> WindowDialog:
	return $edit_comp_dialog as WindowDialog

func _get_comps() -> Array:
	match component_type:
		GEMEditorPlugin.ComponentType.TRIGGER:
			return event.triggers
		GEMEditorPlugin.ComponentType.CONDITION:
			return event.conditions
		GEMEditorPlugin.ComponentType.ACTION:
			return event.actions
		_:
			assert(false)
			return []

func _append_comp(comp):
	var undo_redo : UndoRedo = GEMEditorPlugin.instance().get_undo_redo()
	if comp is GEMTrigger:
		var idx := event.triggers.size()
		undo_redo.create_action(tr("Add trigger"))
		undo_redo.add_do_method(event, "append_trigger", comp)
		undo_redo.add_undo_method(event, "remove_trigger", idx)
		undo_redo.commit_action()
	elif comp is GEMCondition:
		var idx := event.conditions.size()
		undo_redo.create_action(tr("Add condition"))
		undo_redo.add_do_method(event, "append_condition", comp)
		undo_redo.add_undo_method(event, "remove_condition", idx)
		undo_redo.commit_action()
	elif comp is GEMAction:
		var idx := event.actions.size()
		undo_redo.create_action(tr("Add action"))
		undo_redo.add_do_method(event, "append_action", comp)
		undo_redo.add_undo_method(event, "remove_action", idx)
		undo_redo.commit_action()
	else:
		assert(false)

#################
# private stuff #
#################
func __setup():
	.__setup()
	$edit_comp_dialog.component_type = component_type
	
	match component_type:
		GEMEditorPlugin.ComponentType.TRIGGER:
			$label.text = tr("Triggers")
		GEMEditorPlugin.ComponentType.CONDITION:
			$label.text = tr("Conditions")
		GEMEditorPlugin.ComponentType.ACTION:
			$label.text = tr("Actions")

###########
# setters #
###########
func _set_component_type(value : int):
	if value != component_type:
		component_type = value
		__setup()
