tool
extends ScrollContainer

const __META_PROPERTY_TYPE = "__property_type__"

onready var cnt_editors := $margin_container/cnt_editors

var __controls := {}

func from_component(comp):
	call_deferred("__from_component", comp) # always defer to make sure we can add children

func to_component(undo_redo : UndoRedo, comp):
	for prop in comp._get_editor_properties():
		var ctrl : Control = __controls[prop["property"]]
		if ctrl:
			if ctrl.get_meta(__META_PROPERTY_TYPE) != prop["type"]:
				assert(false)
				continue
			undo_redo.add_do_property(comp, prop["property"], __get_property_value(ctrl))
			undo_redo.add_undo_property(comp, prop["property"], comp.get(prop["property"]))

func __create_property_control(comp, prop : Dictionary) -> Control:
	var prop_name = prop.get("label", prop["property"].capitalize())
	var hints : Dictionary = prop.get("type_hints", {})
	match prop["type"]:
		TYPE_BOOL:
			var lbl := Label.new()
			lbl.text = "%s:" % prop_name
			cnt_editors.add_child(lbl)
			
			var checkbox := CheckBox.new()
			
			checkbox.size_flags_horizontal = Control.SIZE_EXPAND_FILL
			checkbox.pressed = comp.get(prop["property"])
			
			cnt_editors.add_child(checkbox)
			return checkbox
		TYPE_NODE_PATH:
			var lbl := Label.new()
			lbl.text = "%s:" % prop_name
			cnt_editors.add_child(lbl)
			var editor : Control = preload("res://addons/de.mewin.gduibasics/scenes/editor_node_edit.tscn").instance()
			editor.size_flags_horizontal = Control.SIZE_EXPAND_FILL
			editor.selected_node = comp.get(prop["property"])
			editor.node_type_filter = hints.get("node_type", Node)
			cnt_editors.add_child(editor)
			return editor
	return null

func __from_component(comp):
	for child in cnt_editors.get_children():
		child.queue_free()
	
	__controls.clear()
	
	for prop in comp._get_editor_properties():
		var ctrl := __create_property_control(comp, prop)
		if ctrl:
			ctrl.set_meta(__META_PROPERTY_TYPE, prop["type"])
			__controls[prop["property"]] = ctrl

func __get_property_value(ctrl : Control):
	match ctrl.get_meta(__META_PROPERTY_TYPE):
		TYPE_BOOL:
			return ctrl.pressed
		TYPE_NODE_PATH:
			return ctrl.selected_node
		_:
			assert(false)
			return null

signal changed()
