tool
extends ConfirmationDialog

onready var opt_types := $v_box_container/h_box_container/opt_types
onready var cnt_editor := $v_box_container/cnt_editor
var comp setget _set_comp
var editor : Control

export(GEMEditorPlugin.ComponentType) var component_type = GEMEditorPlugin.ComponentType.TRIGGER

#############
# overrides #
#############
func _ready():
	opt_types.component_type = component_type

#################
# private stuff #
#################
func __validate():
	var valid := true
	if !editor || !editor._is_valid():
		valid = false
	get_ok().disabled = !valid

func __update_editor():
	if editor:
		editor.queue_free()
	if comp:
		editor = comp._get_editor()
		cnt_editor.add_child(editor)
		editor.connect("changed", self, "_on_editor_changed")
	else:
		editor = null
	__validate()

###########
# setters #
###########
func _set_comp(value):
	if value != comp:
		comp = value
		opt_types.set_block_signals(true)
		if comp:
			opt_types.comp_type = GEMEditorPlugin.instance().get_comp_type(comp)
		else:
			opt_types.comp_type = null
		opt_types.set_block_signals(false)

############
# handlers #
############
func _on_about_to_show():
	__update_editor()

func _on_opt_types_comp_type_changed():
	var comp_type = opt_types.comp_type
	if editor:
		editor.queue_free()
	if !comp_type:
		comp = null
	else:
		comp = comp_type.new()
	__update_editor()

func _on_editor_changed():
	__validate()
