tool
extends OptionButton

var action_type setget _set_action_type

#############
# overrides #
#############
func _ready():
	var popup := get_popup()
	popup.connect("about_to_show", self, "_on_about_to_show")
	popup.connect("index_pressed", self, "_on_index_pressed")
	
	if action_type:
		text = action_type._get_display_name()
	else:
		text = tr("<select action type>")

###########
# setters #
###########
func _set_action_type(value):
	if value != action_type:
		action_type = value
		if action_type:
			text = action_type._get_display_name()
		else:
			text = tr("<select action type>")
		emit_signal("action_type_changed")

############
# handlers #
############
func _on_about_to_show():
	var popup := get_popup()
	
	popup.clear()
	var action_types : Dictionary = GEMEditorPlugin.instance().get_action_types()
	for type_name in action_types:
		var action_type_ = action_types[type_name]
		var idx := popup.get_item_count()
		popup.add_item(action_type_._get_display_name())
		popup.set_item_metadata(idx, action_type_)

func _on_index_pressed(idx : int):
	_set_action_type(get_popup().get_item_metadata(idx))

###########
# setters #
###########
signal action_type_changed()
