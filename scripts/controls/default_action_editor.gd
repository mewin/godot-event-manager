tool
extends GEMComponentEditor

onready var common_property_editor := $common_property_editor
onready var common_action_editor := $common_action_editor

var action : GEMAction

func _ready():
	if action:
		common_property_editor.from_component(action)
		common_action_editor.from_action(action)

func _apply(undo_redo : UndoRedo) -> bool:
	assert(action)
	undo_redo.create_action(tr("Changed action"))
	common_property_editor.to_component(undo_redo, action)
	common_action_editor.to_action(undo_redo, action)
	undo_redo.commit_action()
	return true

func _on_common_property_editor_changed():
	emit_signal("changed")

func _on_common_action_editor_changed():
	emit_signal("changed")
