tool
extends OptionButton

var operation_type : int = GEMExpressionCondition.Operation.EQUAL setget _set_operation_type

var __OPTIONS = [{
	"text": "=",
	"value": GEMExpressionCondition.Operation.EQUAL
},{
	"text": "\u2260",
	"value": GEMExpressionCondition.Operation.NOT_EQUAL
},{
	"text": "<",
	"value": GEMExpressionCondition.Operation.LESS
},{
	"text": "\u2264",
	"value": GEMExpressionCondition.Operation.LESS_EQUAL
},{
	"text": ">",
	"value": GEMExpressionCondition.Operation.GREATER
},{
	"text": "\u2265",
	"value": GEMExpressionCondition.Operation.GREATER_EQUAL
}]

func _ready():
	clear()
	for opt in __OPTIONS:
		var idx := get_item_count()
		add_item(opt["text"])
		set_item_metadata(idx, opt)
	connect("item_selected", self, "_on_item_selected")

func __operation_type_index(op_type : int):
	for i in range(__OPTIONS.size()):
		if __OPTIONS[i]["value"] == op_type:
			return i
	return -1

func _set_operation_type(value : int):
	if value != operation_type:
		operation_type = value
		select(__operation_type_index(value))

func _on_item_selected(idx : int):
	operation_type = __OPTIONS[idx].value
	emit_signal("operation_type_changed")

signal operation_type_changed()
