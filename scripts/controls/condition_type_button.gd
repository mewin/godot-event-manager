tool
extends OptionButton

var condition_type setget _set_condition_type

#############
# overrides #
#############
func _ready():
	var popup := get_popup()
	popup.connect("about_to_show", self, "_on_about_to_show")
	popup.connect("index_pressed", self, "_on_index_pressed")
	
	if condition_type:
		text = condition_type._get_display_name()
	else:
		text = tr("<select condition type>")

###########
# setters #
###########
func _set_condition_type(value):
	if value != condition_type:
		condition_type = value
		if condition_type:
			text = condition_type._get_display_name()
		else:
			text = tr("<select condition type>")
		emit_signal("condition_type_changed")

############
# handlers #
############
func _on_about_to_show():
	var popup := get_popup()
	
	popup.clear()
	var condition_types : Dictionary = GEMEditorPlugin.instance().get_condition_types()
	for type_name in condition_types:
		var condition_type_ = condition_types[type_name]
		var idx := popup.get_item_count()
		popup.add_item(condition_type_._get_display_name())
		popup.set_item_metadata(idx, condition_type_)

func _on_index_pressed(idx : int):
	_set_condition_type(get_popup().get_item_metadata(idx))

###########
# setters #
###########
signal condition_type_changed()
