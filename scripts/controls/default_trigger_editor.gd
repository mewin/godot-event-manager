tool
extends GEMComponentEditor

onready var common_property_editor := $common_property_editor
onready var common_trigger_editor := $common_trigger_editor

var trigger : GEMTrigger

func _ready():
	if trigger:
		common_property_editor.from_component(trigger)
		common_trigger_editor.from_trigger(trigger)

func _apply(undo_redo : UndoRedo) -> bool:
	assert(trigger)
	undo_redo.create_action(tr("Changed trigger"))
	common_property_editor.to_component(undo_redo, trigger)
	common_trigger_editor.to_trigger(undo_redo, trigger)
	undo_redo.commit_action()
	return true

func _on_common_property_editor_changed():
	emit_signal("changed")

func _on_common_trigger_editor_changed():
	emit_signal("changed")
