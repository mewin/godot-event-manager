tool
extends GEMComponentEditor

onready var tree_node := $grid_container/tree_node
onready var tree_property := $grid_container/tree_property
onready var edt_node := $grid_container/edt_node
onready var edt_property := $grid_container/edt_property
onready var edt_value := $edt_value
onready var common_action_editor := $common_action_editor

var action : GEMSetPropertyAction

func _ready():
	if action:
		tree_node.selected_node = action.node
		edt_node.text = str(action.node)
		__update_property_list()
		
		edt_property.text = action.property
		tree_property.selected_property = action.property
		edt_value.text = action.value
		common_action_editor.from_action(action)

func _is_valid() -> bool:
	return true

func _apply(undo_redo : UndoRedo) -> bool:
	assert(action)
	undo_redo.create_action(tr("Set property action changed"))
	undo_redo.add_do_property(action, "node", NodePath(edt_node.text))
	undo_redo.add_do_property(action, "property", edt_property.text)
	undo_redo.add_do_property(action, "value", edt_value.text)
	undo_redo.add_undo_property(action, "node", action.node)
	undo_redo.add_undo_property(action, "property", action.property)
	undo_redo.add_undo_property(action, "value", action.value)
	common_action_editor.to_action(undo_redo, action)
	undo_redo.commit_action()
	return true

func __compare_properties(prop0 : Dictionary, prop1 : Dictionary):
	return prop0.name < prop1.name

func __update_property_list():
	var node := get_tree().edited_scene_root.get_node_or_null(tree_node.selected_node)
	if node:
		var properties := node.get_property_list()
		properties.sort_custom(self, "__compare_properties")
		GDBAlgorithm.remove_if(properties, 'return prop.get("usage", 0) & PROPERTY_USAGE_EDITOR == 0', ["prop"])
		tree_property.properties = properties
	else:
		tree_property.properties = []

func _on_tree_node_selected_node_changed():
	if !edt_node:
		return
	__update_property_list()
	edt_node.text = str(tree_node.selected_node)
	emit_signal("changed")

func _on_edt_value_text_changed(new_text : String):
	emit_signal("changed")

func _on_edt_node_text_changed(new_text : String):
	if !tree_node:
		return
	var root := get_tree().edited_scene_root
	var node := root.get_node_or_null(new_text)
	if node:
		tree_node.set_block_signals(true)
		tree_node.selected_node = root.get_path_to(node)
		tree_node.set_block_signals(false)
		__update_property_list()
	emit_signal("changed")

func _on_edt_property_text_changed(new_text : String):
	if !tree_property:
		return
	tree_property.set_block_signals(true)
	tree_property.selected_property = new_text
	tree_property.set_block_signals(false)
	emit_signal("changed")

func _on_tree_property_selected_property_changed():
	if !edt_property:
		return
	edt_property.text = tree_property.selected_property
	emit_signal("changed")

func _on_common_action_editor_changed():
	emit_signal("changed")
