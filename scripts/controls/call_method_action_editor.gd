tool
extends GEMComponentEditor

onready var tree_node := $grid_container/tree_node
onready var tree_method := $grid_container/tree_method
onready var edt_node := $grid_container/edt_node
onready var edt_method := $grid_container/edt_method
onready var edt_parameter := $edt_parameter
onready var common_action_editor := $common_action_editor

var action : GEMCallMethodAction

func _ready():
	if action:
		tree_node.selected_node = action.node
		edt_node.text = str(action.node)
		__update_method_list()
		
		edt_method.text = action.method
		tree_method.selected_method = action.method
		common_action_editor.from_action(action)
		# edt_parameters.text = action.parameters
	

func _is_valid() -> bool:
	return true

func _apply(undo_redo : UndoRedo) -> bool:
	assert(action)
	undo_redo.create_action(tr("Call method action changed"))
	undo_redo.add_do_property(action, "node", NodePath(edt_node.text))
	undo_redo.add_do_property(action, "method", edt_method.text)
	undo_redo.add_undo_property(action, "node", action.node)
	undo_redo.add_undo_property(action, "method", action.method)
	common_action_editor.to_action(undo_redo, action)
	undo_redo.commit_action()
	# action.parameters = edt_parameters.text
	return true

func __compare_methods(method0 : Dictionary, method1 : Dictionary):
	return method0["name"] < method1["name"]

func __update_method_list():
	var node := get_tree().edited_scene_root.get_node_or_null(tree_node.selected_node)
	if node:
		var methods := node.get_method_list()
		methods.sort_custom(self, "__compare_methods")
		tree_method.methods = methods
	else:
		tree_method.methods = []

func _on_tree_node_selected_node_changed():
	if !edt_node:
		return
	__update_method_list()
	edt_node.text = str(tree_node.selected_node)
	emit_signal("changed")

func _on_edt_value_text_changed(new_text : String):
	emit_signal("changed")

func _on_edt_node_text_changed(new_text : String):
	if !tree_node:
		return
	var root := get_tree().edited_scene_root
	var node := root.get_node_or_null(new_text)
	if node:
		tree_node.set_block_signals(true)
		tree_node.selected_node = root.get_path_to(node)
		tree_node.set_block_signals(false)
		__update_method_list()
	emit_signal("changed")

func _on_edt_method_text_changed(new_text : String):
	if !tree_method:
		return
	tree_method.set_block_signals(true)
	tree_method.selected_method = new_text
	tree_method.set_block_signals(false)
	emit_signal("changed")

func _on_tree_method_selected_method_changed():
	if !edt_method:
		return
	edt_method.text = tree_method.selected_method
	emit_signal("changed")

func _on_common_action_editor_changed():
	emit_signal("changed")
