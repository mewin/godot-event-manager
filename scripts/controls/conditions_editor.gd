tool
extends "res://addons/de.mewin.godot-event-manager/scripts/controls/event_comp_editor.gd"

func _get_tree() -> Tree:
	return $tree_conditions as Tree

func _get_edit_dialog() -> WindowDialog:
	return $edit_comp_dialog as WindowDialog

func _get_comps():
	return event.conditions

func _append_comp(comp):
	event.append_condition(comp)
