tool
extends OptionButton

var event : GEMEvent setget _set_event
var __events : Array

#############
# overrides #
#############
func _ready():
	__update()
	
	var popup := get_popup()
	popup.connect("about_to_show", self, "_on_about_to_show")
	popup.connect("index_pressed", self, "_on_index_pressed")

################
# public stuff #
################
func update_text():
	__update()

#################
# private stuff #
#################
func __update():
	if event:
		text = event.label
	else:
		text = tr("<select event>")

###########
# setters #
###########
func _set_event(value : GEMEvent):
	if value != event:
		if event:
			event.disconnect("label_changed", self, "_on_event_label_changed")
		event = value
		if event:
			event.connect("label_changed", self, "_on_event_label_changed")
		__update()

############
# handlers #
############
func _on_about_to_show():
	var event_controller : GEMEventController = GEMEditorPlugin.instance().find_event_controller()
	if event_controller:
		__events = event_controller.events
	else:
		__events = []
	
	var popup := get_popup()
	popup.clear()
	
	for event in __events:
		popup.add_item(event.label)

func _on_index_pressed(idx : int):
	if idx >= __events.size():
		return
	
	var event_ = __events[idx]
	if event_ != event:
		event = event_
		__update()
		emit_signal("event_selected")

func _on_event_label_changed():
	assert(event)
	__update()

###########
# signals #
###########
signal event_selected()
