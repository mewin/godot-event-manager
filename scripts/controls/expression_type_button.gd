tool
extends OptionButton

var expression_type : int = GEMExpressionCondition.Type.STATIC setget _set__expression_type

var __OPTIONS = [{
	"text": "Event Parameter",
	"value": GEMExpressionCondition.Type.EVENT_PARAMETER
},{
	"text": "Expression",
	"value": GEMExpressionCondition.Type.STATIC
}]

func _ready():
	for opt in __OPTIONS:
		var idx := get_item_count()
		add_item(opt["text"])
		set_item_metadata(idx, opt)
	connect("item_selected", self, "_on_item_selected")

func __expression_type_index(expr_type : int):
	for i in range(__OPTIONS.size()):
		if __OPTIONS[i]["value"] == expr_type:
			return i
	return -1

func _set__expression_type(value : int):
	if value != expression_type:
		expression_type = value
		select(__expression_type_index(value))

func _on_item_selected(idx : int):
	expression_type = __OPTIONS[idx].value
	emit_signal("expression_type_changed")

signal expression_type_changed()
