tool
extends Control

onready var chk_disabled := $chk_disabled

func from_action(action : GEMAction):
	chk_disabled.pressed = action.disabled

func to_action(undo_redo : UndoRedo, action : GEMAction):
	undo_redo.add_do_property(action, "disabled", chk_disabled.pressed)
	undo_redo.add_undo_property(action, "disabled", action.disabled)
	
	# apparently this isnt emitted automatically
	undo_redo.add_do_method(action, "emit_signal", "changed")
	undo_redo.add_undo_method(action, "emit_signal", "changed")

func _on_chk_toggled(button_pressed):
	emit_signal("changed")

signal changed()
