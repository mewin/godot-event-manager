tool
extends Control

class __ComponentMoveAction:
	extends Object
	
	var event : GEMEvent
	var array : Array
	var idx_from : int
	var idx_to : int
	
	func __is_valid() -> bool:
		return idx_from >= 0 && idx_from < array.size() \
			&& idx_to >= 0 && idx_to < array.size() \
			&& idx_from != idx_to
	
	func do():
		if !__is_valid():
			return
		var ele = array[idx_from]
		array.remove(idx_from)
		array.insert(idx_to, ele)
		event.emit_signal("component_order_changed")
	
	func undo():
		if !__is_valid():
			return
		var ele = array[idx_to]
		array.remove(idx_to)
		array.insert(idx_from, ele)
		event.emit_signal("component_order_changed")

class __ComponentRemoveAction:
	extends Object
	
	var event : GEMEvent
	var array : Array
	var idx : int
	var __element
	
	func __is_valid(undo := false) -> bool:
		return idx >= 0 && idx < array.size() + (1 if undo else 0)
	
	func do():
		if !__is_valid():
			return
		__element = array[idx]
		array.remove(idx)
		event.emit_signal("component_order_changed")
	
	func undo():
		if !__is_valid(true) || !__element:
			return
		array.insert(idx, __element)
		event.emit_signal("component_order_changed")
	

var event : GEMEvent setget _set_event

var __tree : Tree
var __edit_dialog : WindowDialog

var __btn_idx_edit : int
var __btn_idx_up : int
var __btn_idx_down : int
var __btn_idx_remove : int

func _ready():
	call_deferred("__setup")

func _get_tree() -> Tree:
	assert(false)
	return null

func _get_edit_dialog() -> WindowDialog:
	assert(false)
	return null

func _get_comps() -> Array:
	assert(false)
	return []

func _append_comp(comp):
	assert(false)

func __setup():
	__tree = _get_tree()
	__edit_dialog = _get_edit_dialog()
	
	assert(__tree)
	assert(__edit_dialog)
	
	__fill()
	
	if !__tree.is_connected("button_pressed", self, "_on_tree_button_pressed"):
		__tree.connect("button_pressed", self, "_on_tree_button_pressed")

func __fill():
	__tree.clear()
	
	__tree.create_item() # root

	if !event:
		return

	var comps := _get_comps()
	for comp in comps:
		if !comp:
			printerr("invalid object in item list")
			continue
		var itm : TreeItem = __tree.create_item()
		itm.set_text(0, str(comp))
		itm.set_metadata(0, comp)
		
		__btn_idx_edit = itm.get_button_count(0)
		itm.add_button(0, preload("res://addons/de.mewin.godot-event-manager/images/edit.svg"))
		
		__btn_idx_down = itm.get_button_count(0)
		itm.add_button(0, preload("res://addons/de.mewin.godot-event-manager/images/arrow_down.svg"))
		itm.set_button_disabled(0, __btn_idx_down, comp == comps[comps.size() - 1])
		
		__btn_idx_up = itm.get_button_count(0)
		itm.add_button(0, preload("res://addons/de.mewin.godot-event-manager/images/arrow_up.svg"))
		itm.set_button_disabled(0, __btn_idx_up, comp == comps[0])
		
		__btn_idx_remove = itm.get_button_count(0)
		itm.add_button(0, preload("res://addons/de.mewin.godot-event-manager/images/trash.svg"))
		
		if comp.disabled:
			itm.set_custom_color(0, Color.darkgray)
		
		if !comp.is_connected("changed", self, "_on_component_changed"):
			comp.connect("changed", self, "_on_component_changed")

func _set_event(value):
	if value != event:
		if event:
			event.disconnect("component_order_changed", self, "_on_event_component_order_changed")
		event = value
		if event:
			event.connect("component_order_changed", self, "_on_event_component_order_changed")
		if __tree: # only when already set up
			__fill()

func _on_btn_add_pressed():
	assert(event)
	__edit_dialog.window_title = tr("Add ...")
	__edit_dialog.comp = null
	__edit_dialog.popup_centered_ratio()

func _on_edit_comp_dialog_confirmed():
	assert(__edit_dialog.comp)
	if __edit_dialog.editor:
		var undo_redo : UndoRedo = GEMEditorPlugin.instance().get_undo_redo()
		__edit_dialog.editor._apply(undo_redo)
	var comps := _get_comps()
	if !comps.has(__edit_dialog.comp):
		_append_comp(__edit_dialog.comp)
		__fill() # not required when editing, see _on_component_changed

func _on_tree_button_pressed(item : TreeItem, column : int, id : int):
	assert(event)
	if !item || column != 0:
		return
	
	var comp = item.get_metadata(0)
	if !comp:
		return
	
	match id:
		__btn_idx_edit:
			__edit_dialog.window_title = tr("Edit ...")
			__edit_dialog.comp = comp
			__edit_dialog.popup_centered_ratio()
		__btn_idx_down:
			var undo_redo : UndoRedo = GEMEditorPlugin.instance().get_undo_redo()
			var comps := _get_comps()
			var idx := comps.find(comp)
			assert(idx > -1)
			if idx < comps.size() - 1:
				var mover := __ComponentMoveAction.new()
				mover.event = event
				mover.array = comps
				mover.idx_from = idx
				mover.idx_to = idx + 1
				undo_redo.create_action(tr("Move down"))
				undo_redo.add_do_method(mover, "do")
				undo_redo.add_undo_method(mover, "undo")
				undo_redo.add_do_reference(mover)
				undo_redo.add_undo_reference(mover)
				undo_redo.commit_action()
		__btn_idx_up:
			var undo_redo : UndoRedo = GEMEditorPlugin.instance().get_undo_redo()
			var comps := _get_comps()
			var idx := comps.find(comp)
			assert(idx > -1)
			if idx > 0:
				var mover := __ComponentMoveAction.new()
				mover.event = event
				mover.array = comps
				mover.idx_from = idx
				mover.idx_to = idx - 1
				undo_redo.create_action(tr("Move up"))
				undo_redo.add_do_method(mover, "do")
				undo_redo.add_undo_method(mover, "undo")
				undo_redo.add_do_reference(mover)
				undo_redo.add_undo_reference(mover)
				undo_redo.commit_action()
		__btn_idx_remove:
			var undo_redo : UndoRedo = GEMEditorPlugin.instance().get_undo_redo()
			var comps := _get_comps()
			var idx := comps.find(comp)
			assert(idx > -1)
			if idx >= 0:
				var remover := __ComponentRemoveAction.new()
				remover.event = event
				remover.array = comps
				remover.idx = idx
				undo_redo.create_action(tr("Remove"))
				undo_redo.add_do_method(remover, "do")
				undo_redo.add_undo_method(remover, "undo")
				undo_redo.add_do_reference(remover)
				undo_redo.add_undo_reference(remover)
				undo_redo.commit_action()

func _on_component_changed():
	__fill()

func _on_event_component_order_changed():
	__fill()
