tool
extends Control

onready var chk_oneshot := $chk_oneshot
onready var chk_deferred := $chk_deferred
onready var chk_disabled := $chk_disabled

func from_trigger(trigger : GEMTrigger):
	chk_oneshot.pressed = trigger.oneshot
	chk_deferred.pressed = trigger.deferred
	chk_disabled.pressed = trigger.disabled

func to_trigger(undo_redo : UndoRedo, trigger : GEMTrigger):
	undo_redo.add_do_property(trigger, "oneshot", chk_oneshot.pressed)
	undo_redo.add_do_property(trigger, "deferred", chk_deferred.pressed)
	undo_redo.add_do_property(trigger, "disabled", chk_disabled.pressed)
	undo_redo.add_undo_property(trigger, "oneshot", trigger.oneshot)
	undo_redo.add_undo_property(trigger, "deferred", trigger.deferred)
	undo_redo.add_undo_property(trigger, "disabled", trigger.disabled)
	
	# apparently this isnt emitted automatically
	undo_redo.add_do_method(trigger, "emit_signal", "changed")
	undo_redo.add_undo_method(trigger, "emit_signal", "changed")

func _on_chk_toggled(button_pressed):
	emit_signal("changed")

signal changed()
