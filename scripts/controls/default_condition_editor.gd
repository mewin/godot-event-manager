tool
extends GEMComponentEditor

onready var common_property_editor := $common_property_editor
onready var common_condition_editor := $common_condition_editor

var condition : GEMCondition

func _ready():
	if condition:
		common_property_editor.from_component(condition)
		common_condition_editor.from_condition(condition)

func _apply(undo_redo : UndoRedo) -> bool:
	assert(condition)
	undo_redo.create_action(tr("Changed condition"))
	common_property_editor.to_component(undo_redo, condition)
	common_condition_editor.to_condition(undo_redo, condition)
	undo_redo.commit_action()
	return true

func _on_common_property_editor_changed():
	emit_signal("changed")

func _on_common_condition_editor_changed():
	emit_signal("changed")
