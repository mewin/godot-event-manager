tool
extends Resource

export var disabled := false

var __owner_id := 0

func _associated_to(node : Node) -> bool:
	return false

# property -> String
# type -> int (TYPE_*)
# label -> String (optional, defaults to property)
# type_hints -> Dictionary (optional)
func _get_editor_properties() -> Array:
	return []

# returns an array of NodePaths
func _get_related_nodes() -> Array:
	return []

func get_owner() -> GEMEvent:
	if !__owner_id:
		return null
	else:
		return instance_from_id(__owner_id) as GEMEvent

func set_owner(owner : GEMEvent):
	if !owner:
		__owner_id = 0
	else:
		__owner_id = owner.get_instance_id()
