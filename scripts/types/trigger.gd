tool
extends "res://addons/de.mewin.godot-event-manager/scripts/types/component.gd"

class_name GEMTrigger

export var oneshot := false
export var deferred := false

func _prepare():
	pass

func _to_string() -> String:
	return _get_display_name()

func _get_editor() -> GEMComponentEditor:
	var editor := load("res://addons/de.mewin.godot-event-manager/scenes/editors/default_trigger_editor.tscn").instance() as GEMComponentEditor
	editor.trigger = self
	return editor

static func _get_display_name() -> String:
	return _get_type_name()

static func _get_type_name() -> String:
	printerr("GEMTrigger: call to abstract function _get_type_name()")
	assert(false)
	return ""
