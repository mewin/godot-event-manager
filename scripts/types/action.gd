tool
extends "res://addons/de.mewin.godot-event-manager/scripts/types/component.gd"

class_name GEMAction

func _to_string() -> String:
	return _get_display_name()

func _apply():
	pass

func _get_editor() -> GEMComponentEditor:
	var editor := load("res://addons/de.mewin.godot-event-manager/scenes/editors/default_action_editor.tscn").instance() as GEMComponentEditor
	editor.action = self
	return editor

static func _get_display_name() -> String:
	return _get_type_name()

static func _get_type_name() -> String:
	printerr("GEMAction: call to abstract function _get_type_name()")
	assert(false)
	return ""
