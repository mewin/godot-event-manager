tool
extends GEMTrigger

class_name GEMSignalTrigger

export(NodePath) var node = NodePath()
export(String) var signal_name := ""

func _prepare():
	var scene_root := get_owner().get_root_node()
	if !scene_root || !node || !signal_name:
		return
	
	var node_ := scene_root.get_node_or_null(node)
	if !node_:
		printerr("GEMSignalTrigger: node not found")
		return
	if !node_.is_connected(signal_name, self, "_on_signal"):
		node_.connect(signal_name, self, "_on_signal")

func _to_string() -> String:
	var node_name := tr("<unset>")
	var signal_name_ := signal_name if signal_name else tr("<unset>")
	if node && !node.is_empty():
		node_name = node.get_name(node.get_name_count() - 1)
	return tr("{node}.{signal} emitted").format({
		"node": node_name,
		"signal": signal_name_
	})

func _get_editor() -> GEMComponentEditor:
	var editor : GEMComponentEditor = load("res://addons/de.mewin.godot-event-manager/scenes/editors/signal_trigger_editor.tscn").instance()
	editor.trigger = self
	return editor

# returns an array of NodePaths
func _get_related_nodes() -> Array:
	return [node]

func _on_signal(p0 = null, p1 = null, p2 = null, p3 = null):
	# TODO: set signal/parameter variables
	get_owner().execute(self)

static func _get_display_name() -> String:
	return "Signal Emitted"

static func _get_type_name() -> String:
	return "SignalTrigger"
