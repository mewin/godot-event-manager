tool
extends "res://addons/de.mewin.godot-event-manager/scripts/types/component.gd"

class_name GEMCondition

func _to_string() -> String:
	return _get_display_name()

func _test() -> bool:
	return true

func _get_editor() -> GEMComponentEditor:
	var editor := load("res://addons/de.mewin.godot-event-manager/scenes/editors/default_condition_editor.tscn").instance() as GEMComponentEditor
	editor.condition = self
	return editor

static func _get_display_name() -> String:
	printerr("GEMCondition: call to abstract function _get_display_name()")
	assert(false)
	return ""

static func _get_type_name() -> String:
	printerr("GEMCondition: call to abstract function _get_type_name()")
	assert(false)
	return ""
