tool
extends GEMCondition

class_name GEMExpressionCondition

enum Operation {
	EQUAL,
	NOT_EQUAL,
	LESS,
	LESS_EQUAL,
	GREATER,
	GREATER_EQUAL
}

enum Type {
	EVENT_PARAMETER,
	STATIC
}

export(String) var expression0 := ""
export(String) var expression1 := ""
#export(Type) var expression_type0 = Type.EVENT_PARAMETER
#export(Type) var expression_type1 = Type.STATIC
export(Operation) var operation = Operation.EQUAL

func _to_string() -> String:
	var op_symbol := "="
	match operation:
		Operation.NOT_EQUAL:
			op_symbol = "\u2260"
		Operation.LESS:
			op_symbol = "<"
		Operation.LESS_EQUAL:
			op_symbol = "\u2264"
		Operation.GREATER:
			op_symbol = ">"
		Operation.GREATER_EQUAL:
			op_symbol = "\u2265"
	return "%s %s %s" % [
		expression0 if expression0 else tr("<unset>"),
		op_symbol,
		expression1 if expression1 else tr("<unset>")
	]

func _test() -> bool:
	assert(get_owner())
	
	var val0 = __parse_expression(expression0)
	var val1 = __parse_expression(expression1)
	var common_type = __common_type(val0, val1)
	var v0 = convert(val0, common_type)
	var v1 = convert(val1, common_type)
	
	match operation:
		Operation.EQUAL:
			return v0 == v1
		Operation.NOT_EQUAL:
			return v0 != v1
		Operation.LESS:
			return v0 < v1
		Operation.LESS_EQUAL:
			return v0 <= v1
		Operation.GREATER:
			return v0 > v1
		Operation.GREATER_EQUAL:
			return v0 >= v1
	
	return false

func _get_editor() -> GEMComponentEditor:
	var editor : GEMComponentEditor = load("res://addons/de.mewin.godot-event-manager/scenes/editors/expression_condition_editor2.tscn").instance()
	editor.condition = self
	return editor

func __common_type(val0, val1) -> int:
	var type0 := typeof(val0)
	var type1 := typeof(val1)
	
	if type0 == type1:
		return type0
	if type0 > type1:
		var tmp = type0
		type0 = type1
		type1 = tmp
		
		tmp = val0
		val0 = val1
		val1 = tmp
	
	match type0:
		TYPE_BOOL:
			if type1 == TYPE_INT || type1 == TYPE_REAL:
				return type1
		TYPE_INT:
			if type1 == TYPE_REAL:
				return TYPE_REAL
			elif type1 == TYPE_STRING:
				if val1.is_valid_integer():
					return TYPE_INT
				elif val1.is_valid_float():
					return TYPE_REAL
		TYPE_REAL:
			if type1 == TYPE_STRING && val1.is_valid_float():
				return TYPE_REAL
	return TYPE_STRING

func __parse_expression(expression : String):
	return expression # TODO

static func _get_display_name() -> String:
	return "Expression"

static func _get_type_name() -> String:
	return "ExpressionCondition"
