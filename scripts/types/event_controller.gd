tool
extends Node

class_name GEMEventController, "res://addons/de.mewin.godot-event-manager/images/gem.svg"

export(Array, Resource) var events := [].duplicate()

func _ready():
	if Engine.editor_hint:
		return
	for event in events:
		event.set_root_node(owner)
		for trigger in event.triggers:
			trigger._prepare()

func add_event(event : GEMEvent, idx := -1):
	if idx < 0 || idx >= events.size():
		events.append(event)
	else:
		events.insert(idx, event)
	emit_signal("events_changed")

func erase_event(event : GEMEvent):
	events.erase(event)
	emit_signal("events_changed")

signal events_changed()
