extends Reference

class_name GEMConditionType

func _instance() -> GEMCondition:
	return _get_type().new() as GEMCondition

func _get_type():
	printerr("GEMConditionType: call to abstract function _get_type()")
	assert(false)
	return null
