tool
extends Resource

class_name GEMEvent

export(String) var label := "Event" setget _set_label
export(Array, Resource) var triggers := Array().duplicate() setget _set_triggers
export(Array, Resource) var conditions := Array().duplicate() setget _set_conditions
export(Array, Resource) var actions := Array().duplicate() setget _set_actions
var parameters := {}
var parameter_values := {}
var __root_node_id : int

func append_trigger(trigger):
	trigger.set_owner(self)
	triggers.append(trigger)
	emit_signal("component_order_changed")

func append_condition(condition):
	condition.set_owner(self)
	conditions.append(condition)
	emit_signal("component_order_changed")

func append_action(action):
	action.set_owner(self)
	actions.append(action)
	emit_signal("component_order_changed")

func remove_trigger(idx : int):
	triggers.remove(idx)
	emit_signal("component_order_changed")

func remove_condition(idx : int):
	conditions.remove(idx)
	emit_signal("component_order_changed")

func remove_action(idx : int):
	actions.remove(idx)
	emit_signal("component_order_changed")

func execute(trigger = null):
	if trigger:
		if trigger.disabled:
			return
		elif trigger.deferred:
			call_deferred("execute") # without trigger -> do not deferr again
			return
	
	for condition in conditions:
		if !condition.disabled && !condition._test():
			return
	
	# check oneshot only after conditions have been checked
	if trigger && trigger.oneshot:
		trigger.disabled = true
	
	for action in actions:
		if !action.disabled:
			action._apply()

func get_root_node() -> Node:
	if !__root_node_id:
		return null
	return instance_from_id(__root_node_id) as Node

func set_root_node(node : Node):
	if node:
		__root_node_id = node.get_instance_id()
	else:
		__root_node_id = 0

func _set_label(value : String):
	if value != label:
		label = value
		emit_signal("label_changed")

func _set_triggers(triggers_ : Array):
	if triggers_ != triggers:
		triggers = triggers_
		for trigger in triggers:
			trigger.set_owner(self)

func _set_conditions(conditions_ : Array):
	if conditions_ != conditions:
		conditions = conditions_
		for condition in conditions:
			condition.set_owner(self)

func _set_actions(actions_ : Array):
	if actions_ != actions:
		actions = actions_
		for action in actions:
			action.set_owner(self)

static func make_parameter(name : String, type : int) -> Dictionary:
	return {
		"name": name,
		"type": type
	}

signal label_changed()
signal component_order_changed()
