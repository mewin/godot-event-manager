tool
extends Control

class_name GEMComponentEditor

func _is_valid() -> bool:
	return true

func _apply(undo_redo : UndoRedo) -> bool:
	return true

signal changed()
