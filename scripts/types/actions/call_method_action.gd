tool
extends GEMAction

class_name GEMCallMethodAction

export(NodePath) var node = NodePath()
export(String) var method := ""
export var parameters := []

func _apply():
	var scene_root := get_owner().get_root_node()
	if !scene_root || !node || !method:
		return
	var node_ := scene_root.get_node_or_null(node)
	if !node_:
		printerr("GEMCallMethodAction: node not found")
		return
	var types := GDBUtility.get_method_arg_types(node, method)
	var args := parameters.duplicate()
	for i in range(min(args.size(), types.size())):
		if types[i] != TYPE_NIL:
			args[i] = convert(args[i], types[i])
	node_.callv(method, args)

func _to_string() -> String:
	var node_name := tr("<unset>")
	var method_name := method if method else tr("<unset>")
	if node && !node.is_empty():
		node_name = node.get_name(node.get_name_count() - 1)
	return tr("Call {node}.{method}()").format({
		"node": node_name,
		"method": method_name
	})

func _get_editor() -> GEMComponentEditor:
	var editor : GEMComponentEditor = load("res://addons/de.mewin.godot-event-manager/scenes/editors/call_method_action_editor.tscn").instance()
	editor.action = self
	return editor

# returns an array of NodePaths
func _get_related_nodes() -> Array:
	return [node]

static func _get_display_name() -> String:
	return "Call Method"

static func _get_type_name() -> String:
	return "CallMethodAction"
