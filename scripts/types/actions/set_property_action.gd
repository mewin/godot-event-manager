tool
extends GEMAction

class_name GEMSetPropertyAction

export(NodePath) var node := NodePath()
export var property := ""
export var value := ""

func _apply():
	var scene_root := get_owner().get_root_node()
	if !scene_root || !node || !property:
		return
	var node_ := scene_root.get_node_or_null(node)
	if !node_:
		printerr("GEMSetPropertyAction: node not found")
		return
	var type := GDBUtility.get_property_type(node_, property)
	var value_ = value
	if type != TYPE_STRING:
		value_ = convert(value_, type)
	node_.set(property, value_)

func _to_string() -> String:
	var node_name := tr("<unset>")
	var property_name := property if property else tr("<unset>")
	if !node.is_empty():
		node_name = node.get_name(node.get_name_count() - 1)
	return tr("Set {node}.{property} to \"{value}\"").format({
		"node": node_name,
		"property": property_name,
		"value": value
	})

func _get_editor() -> GEMComponentEditor:
	var editor : GEMComponentEditor = load("res://addons/de.mewin.godot-event-manager/scenes/editors/set_property_action_editor.tscn").instance()
	editor.action = self
	return editor

# returns an array of NodePaths
func _get_related_nodes() -> Array:
	return [node]

static func _get_display_name() -> String:
	return "Set Property"

static func _get_type_name() -> String:
	return "SetPropertyAction"
